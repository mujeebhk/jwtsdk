package com.jwt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JwtSdkApplication {

	public static void main(String[] args) {
		SpringApplication.run(JwtSdkApplication.class, args);
	}

}
